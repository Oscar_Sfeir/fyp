import {ODataConfiguration} from 'angular-odata-es5';
import {environment} from '../../environments/environment';
import {Injectable} from '@angular/core';

@Injectable()
export class PollingDataConfiguration extends ODataConfiguration {
  baseUrl = environment.odataUrl;
  url = environment.serverUrl;
}
